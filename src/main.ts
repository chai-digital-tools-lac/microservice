import { HttpAdapterHost, NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { config, Endpoint } from 'aws-sdk';
import { AllExceptionsFilter } from './middleware/all-exceptions.filter';
import { ValidationPipe } from '@nestjs/common';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  app.useGlobalPipes(new ValidationPipe());
  const { httpAdapter } = app.get(HttpAdapterHost);
  app.useGlobalFilters(new AllExceptionsFilter(httpAdapter));
  const endpoint = new Endpoint(process.env.AWS_URL);
  config.update({
    s3:{endpoint: endpoint},
    s3ForcePathStyle: true,
    accessKeyId: process.env.AWS_KEY,
    secretAccessKey: process.env.AWS_SECRET,
    region: process.env.AWS_REGION,
  });
  app.enableCors();
  await app.listen(3000);
}
bootstrap();
