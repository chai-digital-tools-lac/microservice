import { BadRequestException, Injectable } from '@nestjs/common';
import { CreateUploadDto } from './dto/create-upload.dto';
import { UpdateUploadDto } from './dto/update-upload.dto';
import { SaveLog } from 'src/core/http/save-log.service';
import { v4 as uuidv4 } from 'uuid';
import { S3 } from 'aws-sdk';
import { DataSend } from './data-send.interface';

@Injectable()
export class UploadService {
  constructor(private saveLog:SaveLog){

  }
  async create(dataBuffer: Buffer, filename: string, createUploadDto: CreateUploadDto,user: number) {
    const data: DataSend = {
      matrix_id: createUploadDto.matrix_id,
      matrix_programming_id: createUploadDto.matrix_programming_id,
      user_id: user,
    };

    const save:boolean = await this.saveLog.saveLog(data, createUploadDto.token);
    if(!save){
      throw new BadRequestException('Error save log programming backend');
    }
    try {
      const s3 = new S3();
      const year = new Date().getFullYear()
      const uploadResult = await s3
        .upload({
          Bucket: process.env.AWS_BUCKET,
          Body: dataBuffer,
          Key: `${createUploadDto.matrix_name}/${year}/${uuidv4()}-${filename}`
        })
        .promise();
        return  { key: uploadResult.Key, url: uploadResult.Location};
    } catch (err) {
      console.log(err)
      throw new BadRequestException(err.message);
    }
  }
}
