import { Controller, Get, Post, Body, Patch, Param, Delete, Headers, HttpException, HttpStatus, UploadedFile, UseInterceptors, BadRequestException } from '@nestjs/common';
import { UploadService } from './upload.service';
import { CreateUploadDto } from './dto/create-upload.dto';
import { UpdateUploadDto } from './dto/update-upload.dto';
import { DBRedisService } from 'src/redis.services';
import { FileInterceptor } from '@nestjs/platform-express';
import { ResponseModel } from './response.model';

@Controller('upload')
export class UploadController {
  constructor(
    private readonly uploadService: UploadService,
    private readonly dBRedisService: DBRedisService,
    ) {}

  @Post()
  @UseInterceptors(FileInterceptor('file'))
  async create(
    @Body() createUploadDto :CreateUploadDto,
    @Headers() headers: Record < string, string >,
    @UploadedFile() file: Express.Multer.File
  ): Promise<any> {
    const userBack = await this.dBRedisService.redis.get(headers.token);
    if(!userBack){
      throw new HttpException('Token unauthorized', HttpStatus.UNAUTHORIZED);
    }
    if(!file){
      throw new BadRequestException('file should not be empty');
    }
    const user = JSON.parse(userBack);
    const result = await this.uploadService.create(
      file.buffer,
      file.originalname,
      createUploadDto,
      user.id
    );
    return new ResponseModel(result); 
  }

}
