import { Module } from '@nestjs/common';
import { UploadService } from './upload.service';
import { UploadController } from './upload.controller';
import { DBRedisService } from 'src/redis.services';
import { SaveLog } from 'src/core/http/save-log.service';
import { HttpModule } from '@nestjs/axios';

@Module({
  imports: [HttpModule],
  controllers: [UploadController],
  providers: [UploadService, DBRedisService, SaveLog]
})
export class UploadModule {}
