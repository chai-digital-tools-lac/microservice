import { IsNotEmpty, IsNumber, IsString } from "@nestjs/class-validator";

export class CreateUploadDto {
    @IsNotEmpty()
    @IsString()
    matrix_id: number;

    @IsNotEmpty()
    @IsString()
    matrix_name: number;

    @IsNotEmpty()
    @IsString()
    matrix_programming_id: number;

    @IsNotEmpty()
    @IsString()
    token: string;
}
