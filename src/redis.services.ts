import { Injectable } from '@nestjs/common'; 
import Redis from 'ioredis';

@Injectable()
export class DBRedisService {
  public redis = new Redis({
    port: Number(process.env.DB_REDIS_PORT), // Redis port
    host: process.env.DB_REDIS_URL, // Redis host
    // username: 'default', // needs Redis >= 6
    // password: process.env.DB_REDIS_PASSWORD,
    db: 0, // Defaults to 0
  });

  constructor() { }
}
