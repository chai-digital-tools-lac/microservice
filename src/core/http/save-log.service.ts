import { HttpService } from '@nestjs/axios';
import { Injectable } from '@nestjs/common';
import { DataSend } from 'src/modules/upload/data-send.interface';

@Injectable()
export class SaveLog {
  
  private url = process.env.BACK_URL
  constructor(private http: HttpService) {}
  
  public async saveLog(data: DataSend, token: string): Promise<boolean>{
    const headers = {
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${token}`
    };
    try {
      const response = await this.http.axiosRef({
          method: 'POST',
          baseURL: this.url,
          url: `matrices-uploads-log`,
          headers: headers,
          data: data
        });
      return response.data.success;
    } catch (err) {
      console.log('Error request backend: '+ err);
      return false;
    }
  }
}