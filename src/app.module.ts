import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { DBRedisService } from './redis.services';
import { UploadModule } from './modules/upload/upload.module';

@Module({
  imports: [ 
    ConfigModule.forRoot({ isGlobal: true }),
    UploadModule,
  ],
  providers: [DBRedisService],
  exports:[DBRedisService]
})
export class AppModule {}
